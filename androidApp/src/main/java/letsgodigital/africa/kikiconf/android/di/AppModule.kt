package letsgodigital.africa.kikiconf.android.di

import letsgodigital.africa.kikiconf.android.KikiConfViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    viewModel { KikiConfViewModel(get()) }
}