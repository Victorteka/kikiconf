package letsgodigital.africa.kikiconf.android

import android.app.Application
import letsgodigital.africa.kikiconf.android.di.appModule
import letsgodigital.africa.kikiconf.di.initKoin
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.logger.Level

class KikiConfApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@KikiConfApplication)
            modules(appModule)
        }
    }
}