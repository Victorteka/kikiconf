package letsgodigital.africa.kikiconf.android

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import letsgodigital.africa.kikiconf.KikiConfRepository
import letsgodigital.africa.kikiconf.fragment.SessionDetails

class KikiConfViewModel(private val repository: KikiConfRepository) : ViewModel() {

    val enabledLanguages: Flow<Set<String>> = repository.enabledLanguages

    val sessions = repository.sessions
    val speakers = repository.speakers
    val rooms = repository.rooms

    suspend fun getSession(sessionId: String): SessionDetails? {
        return repository.getSession(sessionId)
    }

    fun onLanguageChecked(language: String, checked: Boolean){
        repository.updateEnableLanguageSetting(language, checked)
    }
}