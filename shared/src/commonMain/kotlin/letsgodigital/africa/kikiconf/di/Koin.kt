package letsgodigital.africa.kikiconf.di

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.cache.normalized.api.MemoryCacheFactory
import com.apollographql.apollo3.cache.normalized.normalizedCache
import kotlinx.coroutines.ExperimentalCoroutinesApi
import letsgodigital.africa.kikiconf.AppSettings
import letsgodigital.africa.kikiconf.KikiConfRepository
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.module

expect fun platformModule(): Module

fun initKoin(appDeclaration: KoinAppDeclaration = {}) =
    startKoin {
        appDeclaration()
        modules(commonModule(), platformModule())
    }

//Called by IOS client
fun initKoin() = initKoin() { }

@ExperimentalCoroutinesApi
fun commonModule() = module {
    single { KikiConfRepository() }
    single { createApolloClient() }
    single { AppSettings(get()) }
}

fun createApolloClient(): ApolloClient {
    // Create a 10MB memory cache
    val cacheFactory = MemoryCacheFactory(maxSizeBytes = 10 * 1024 * 1024)
    return ApolloClient.Builder()
        .serverUrl("https://kiki-conf.ew.r.appspot.com/graphql")
        .normalizedCache(cacheFactory)
        .build()
}
