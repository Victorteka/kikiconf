package letsgodigital.africa.kikiconf

import android.util.Log
import co.touchlab.kermit.Logger

class Greeting {
    fun greeting(): String {
        Logger.d("Hello from kermit")
        Log.d("Hello ${Platform().platform}", "greeting: ")
        return "Hello, ${Platform().platform}!"
    }
}